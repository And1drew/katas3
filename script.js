function Kata1 (){
    let arr = [];
    for (let i = 1; i <= 20; i++){
        arr.push(" " + i);
    }
    // console.log(arr);
    document.getElementById("K1").innerHTML = arr;
}  
Kata1();

function Kata2 () {
    let arr = [];
    for (let i = 2; i <= 20; i+= 2){
        arr.push(" " + i);
    }
    // console.log(arr);
    document.getElementById("K2").innerHTML = arr;
}
Kata2();

function Kata3 () {
    let arr = [];
    for (let i = 1; i <= 20; i+= 2){
        arr.push(" " + i);
    }
    // console.log(arr);
    document.getElementById("K3").innerHTML = arr;
}
Kata3();

function Kata4 () {
    let arr = [];
    for (let i = 5; i <= 100; i+= 5){
        arr.push(" " + i);
    }
    // console.log(arr);
    document.getElementById("K4").innerHTML = arr;
}
Kata4();

function Kata5 () {
    let arr = [];
    for (let i = 1; i <= 10; i++){
        arr.push(" " + i*i);
    }
    // console.log(arr);
    document.getElementById("K5").innerHTML = arr;
}
Kata5();

function Kata6 (){
    let arr = [];
    for (let i = 20; i >= 1; i--){
        arr.push(" " + i);
    }
    // console.log(arr);
    document.getElementById("K6").innerHTML = arr;
}  
Kata6();

function Kata7 (){
    let arr = [];
    for (let i = 20; i >= 1; i-= 2){
        arr.push(" " + i);
    }
    // console.log(arr);
    document.getElementById("K7").innerHTML = arr;
}  
Kata7();

function Kata8 (){
    let arr = [];
    for (let i = 19; i >= 1; i-= 2){
        arr.push(" " + i);
    }
    // console.log(arr);
    document.getElementById("K8").innerHTML = arr;
}  
Kata8();

function Kata9 () {
    let arr = [];
    for (let i = 100; i > 0; i-= 5){
        arr.push(" " + i);
    }
    // console.log(arr);
    document.getElementById("K9").innerHTML = arr;
}
Kata9();

function Kata10 () {
    let arr =[];
    for(let i = 10; i >= 1; i--){
        arr.push(" "+ i*i);
    }
    // console.log(arr);
    document.getElementById("K10").innerHTML = arr;
}
Kata10();

function Kata11 (){
    let arr = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
    document.getElementById("K11").innerHTML = arr;
}  
Kata11();

function Kata12 (){
    let arr = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
    let sol = [];
    for (i = 0; i < arr.length; i++) {
        if(arr[i]%2 == 0){
            sol.push(" "+ arr[i]);
        }
    }
    document.getElementById("K12").innerHTML = sol;
}
Kata12();

function Kata13 (){
    let arr = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
    let sol = [];
    for (i = 0; i < arr.length; i++) {
        if(arr[i]%2 == 1){
            sol.push(" "+ arr[i]);
        }
    }
    document.getElementById("K13").innerHTML = sol;
}
Kata13();

function Kata14 (){
    let arr = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
    let sol = []
    for (i = 0; i < arr.length; i++) {
        sol.push(" "+ arr[i]*arr[i]);
    }
    document.getElementById("K14").innerHTML = sol;
}
Kata14();

function Kata15() {
    let sum = 0;
    for (i=0; i <=20; i++){
        sum += i;
    }
    document.getElementById("K15").innerHTML = sum;
}
Kata15();

function Kata16() {
    let arr = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
    let sum = 0;
    for (i=0; i < arr.length; i++){
        sum += arr[i];
    }
    document.getElementById("K16").innerHTML = sum;
}
Kata16();

function Kata17() {
    let arr = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
    let sum = Math.min(...arr);
    document.getElementById("K17").innerHTML = sum;
}
Kata17();

function Kata18() {
    let arr = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
    let sum = Math.max(...arr);
    document.getElementById("K18").innerHTML = sum;
}
Kata18();